package socialnetwork.ui;


import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import socialnetwork.config.ApplicationContext;
import socialnetwork.domain.*;
import socialnetwork.domain.validators.*;
import socialnetwork.observers.Observer;
import socialnetwork.repository.Repository;
import socialnetwork.repository.database.FriendRequestsDbRepository;
import socialnetwork.repository.database.FriendshipDbRepository;
import socialnetwork.repository.database.MessageDbRepository;
import socialnetwork.repository.database.UtilizatorDbRepository;
import socialnetwork.service.FriendshipService;
import socialnetwork.service.UtilizatorService;
import socialnetwork.ui.controllers.LoginView;
import socialnetwork.ui.controllers.UserView;

public class FXUi extends Application implements Observer {

    Stage shower;
    Scene a, b;
    UtilizatorService service;
    FriendshipService fr_serv;
    FXMLLoader loader, loader2;

    @Override
    public void start(Stage primaryStage) throws Exception {
        final String url = ApplicationContext.getPROPERTIES().getProperty("database.socialnetwork.url");
        final String username= ApplicationContext.getPROPERTIES().getProperty("database.socialnetwork.username");
        final String pasword= ApplicationContext.getPROPERTIES().getProperty("database.socialnetwork.password");
        Repository<Long, Utilizator> userFileRepository3 =
                new UtilizatorDbRepository(url,username, pasword,  new UtilizatorValidator());
        Repository<Tuple<Long, Long>, Prietenie> friendshipFileRepository3 = new FriendshipDbRepository(url, username, pasword, new PrietenieValidator());
        Repository<Tuple<Utilizator, Utilizator>, FriendRequest> friendreqsDbRepository3 = new FriendRequestsDbRepository(url, username, pasword, new RequestValidator());
        Repository<Long, Message> messageRepository = new MessageDbRepository(url, username, pasword, new MessageValidator());
        AnchorPane pane;

        loader = new FXMLLoader();
        loader2 = new FXMLLoader();
        loader.setLocation(getClass().getResource("/views/login_view.fxml"));
        loader2.setLocation(getClass().getResource("/views/user_view.fxml"));
        pane = loader.load();
        LoginView login = loader.getController();
        AnchorPane user = loader2.load();
        UserView controller_user = loader2.getController();
        service = new UtilizatorService(userFileRepository3, friendshipFileRepository3, messageRepository, friendreqsDbRepository3,new UserNameValidator());
        fr_serv = new FriendshipService(friendshipFileRepository3,userFileRepository3,friendreqsDbRepository3,new UserNameValidator());
        service.add_observer(this);
        login.setService(service);
        login.setService(fr_serv);
        controller_user.setService(service);
        controller_user.setService(fr_serv);
        a = new Scene(pane, 450, 300);
        b = new Scene(user, 450, 300);
        shower = new Stage();
        shower.setScene(a);
        shower.show();
    }

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void observe() {
        if(service.anonymousSession()){
            shower.setScene(a);
        }
        else{
            ((UserView)loader2.getController()).initialize();
            shower.setScene(b);
        }
    }
}
