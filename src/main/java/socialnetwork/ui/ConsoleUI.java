package socialnetwork.ui;

import socialnetwork.domain.*;
import socialnetwork.domain.validators.ValidationException;
import socialnetwork.service.*;
import socialnetwork.service.exceptions.*;

import java.util.ArrayList;
import java.util.Scanner;

public class ConsoleUI implements UI{

    UtilizatorService serv;
    FriendshipService fr_serv;

    public ConsoleUI(UtilizatorService serv, FriendshipService fr_serv){
        this.serv = serv;
        this.fr_serv = fr_serv;
    }

    public void add_user(String first_name, String last_name) {
        try {
            Utilizator a = serv.addUtilizator(new Utilizator(first_name, last_name));
            if(a != null){
                System.out.println("Userul " + a + " mai exista deja");
            }
            else{
                System.out.println("Userul a fost adaugat cu succes");
            }
        }
        catch(Exception e){
            System.out.println(e.toString());
        }
    }

    public void add_pr(Long id, Long id2) {
        try{
            Prietenie a = fr_serv.add_friendship(new Prietenie(new Tuple<>(id, id2)));
            if(a != null){
                System.out.println("Prietenia " + a + " mai exista deja");
            }
            else{
                System.out.println("Relatia a fost adaugata cu succes");
            }
        }
        catch (Exception e){
            System.out.println(e.toString());
        }
    }

    public void loginUser(String username){
        try{
            serv.login_user(username);
            fr_serv.login_user(username);
            System.out.println("User logat cu succes");
        }
        catch(ExistingUserAlreadyLoggedIn | ValidationException e){
            System.out.println(e.toString());
        } catch (Exception e){
            e.printStackTrace();
        }
    }

    public void logout(){
        try{
            serv.logout();
            fr_serv.logout();
            System.out.println("User delogat cu succes");
        }
        catch(NoUserLoggedIn e){
            System.out.println(e.toString());
        }
    }

    public void remove_pr(Long id1, Long id2) {
        try{
            Prietenie x = fr_serv.del_prietenie(new Tuple<>(id1, id2));
            if(x == null){
                System.out.println("Relatia nu exista");
            }
            else{
                System.out.println(x + " a fost sters");
            }
        }
        catch (Exception e){
            System.out.println(e.toString());
        }
    }

    public void remove_user(Long id) {
        try{
            Utilizator x = serv.removeUtilizator(id);
            if(x == null){
                System.out.println("Utilizatorul nu exista");
            }
            else{
                System.out.println(x + " a fost sters");
            }
        }
        catch (Exception e){
            System.out.println(e.toString());
        }
    }
    public void user_all(){
        Iterable<Utilizator> all = serv.getAll();
        all.forEach(utilizator -> System.out.println(utilizator + "\n"));
    }

    public void pr_all(){
        Iterable<Prietenie> all = fr_serv.get_all();
        all.forEach(prietenie -> System.out.println(prietenie + "\n"));
    }

    public void community_number(){
        System.out.println(fr_serv.numberCommunities() + " comunitati existente");
    }

    public void communitySociable(){
        System.out.println(fr_serv.mostSociableComm());
    }

    public void userFriends(){
        try{
            fr_serv.userFriends().forEach(e -> System.out.println(e.toString()));
        }
        catch(NoUserLoggedIn e){
            System.out.println(e.toString());
        }
    }

    public void userFriendsFiltered(int month){
        try{
            fr_serv.userFriendsFiltered(month).forEach(e -> System.out.println(e.toString()));
        }
        catch(NoUserLoggedIn e){
            System.out.println(e.toString());
        }
    }

    public void userFriendsFilteredYear(int year){
        try{
            fr_serv.userFriendsFilteredYear(year).forEach(e -> System.out.println(e.toString()));
        }
        catch(NoUserLoggedIn e){
            System.out.println(e.toString());
        }
    }

    public void getConversation(String username){
        try{
            serv.getConversation(username).forEach(e -> System.out.println(e.toString()));
        }
        catch(NoUserLoggedIn | UserNotExists e){
            System.out.println(e.toString());
        }
    }

    public void sendMessage(String text, ArrayList<String> username){
        try{
            serv.send_message(text, username);
        }
        catch(NoUserLoggedIn | ValidationException | UserNotExists e){
            System.out.println(e.toString());
        }
    }
    public void sendReplyMessage(String text, String id, ArrayList<String> usernames){
        try{
            serv.sendReplyTo(text, Long.valueOf(id), usernames);
        }
        catch(NoUserLoggedIn e){
            System.out.println(e.toString());
        }
    }

    public void processFriendRequests(){
        try{
            for (FriendRequest e : fr_serv.process()) {
                if(e.getId().getLeft().getId() == serv.getCurrentUser().getId()){
                    System.out.println("To " + e.getId().getRight().display_name() + " " + e.getStatus());
                }
                else{
                    System.out.println("From " + e.getId().getLeft().display_name() + " " + e.getStatus());
                }
            }
        }
        catch(NoUserLoggedIn | UserNotExists e){
            System.out.println(e);
        }
    }
    public void sendFrReq(String username){
        try{
            fr_serv.sendFrReq(username);
            System.out.println("Cerere trimisa cu succes");
        }
        catch(NoUserLoggedIn | FriendshipExists | UserNotExists e){
            System.out.println(e);
        }
    }
    public void delFrReq(String username){
        try{
            fr_serv.delFrReq(username);
            System.out.println("Cerere stersa cu succes");
        }
        catch(NoUserLoggedIn | UserNotExists e){
            System.out.println(e);
        }
    }

    public void respond(String username, Status status){
        try{
            fr_serv.respond_req(username, status);
            System.out.println("Ai raspuns la cerere");
        }
        catch(FriendDoesNotExist | RequestProcessed | NoUserLoggedIn e){
            System.out.println(e);
        }
    }

    public void del_mes(String id){
        try{
            Message mes = serv.del_message(Long.valueOf(id));
            if(mes == null)
                System.out.println("Nothing to remove");
            else
                System.out.println("Message " + mes.getText() + " removed");
        }
        catch(NoUserLoggedIn e){
            System.out.println(e);
        }
    }

    @Override
    public void run() {
        Scanner reader = new Scanner(System.in);
        while(true){
            System.out.print( (serv.anonymousSession()) ? "Introduceti comanda > " : serv.getCurrentUser().display_name() + " >> ");
            String message = reader.nextLine();
            String[] args = message.split(";");
            if(args[0].equals("exit")) break;
            switch(args[0]){
                case "add_user":
                    if(args.length == 3)
                        add_user(args[1], args[2]);
                    else
                        System.out.println("Numar invalid de arumente");
                    break;
                case "del_user":
                    if(args.length == 2)
                        remove_user(Long.valueOf(args[1]));
                    else
                        System.out.println("Numar invalid de arumente");
                    break;
                case "add_pr":
                    if(args.length == 3)
                        add_pr(Long.valueOf(args[1]), Long.valueOf(args[2]));
                    else
                        System.out.println("Numar invalid de arumente");
                    break;
                case "del_pr":
                    if(args.length == 3)
                        remove_pr(Long.valueOf(args[1]), Long.valueOf(args[2]));
                    else
                        System.out.println("Numar invalid de arumente");
                    break;
                case "pr_all":
                    pr_all();
                    break;
                case "user_all":
                    user_all();
                    break;
                case "number":
                    community_number();
                    break;
                case "most":
                    communitySociable();
                    break;
                case "login":
                    if(args.length == 2)
                        loginUser(args[1]);
                    else
                        System.out.println("Numar invalid de arumente");
                    break;
                case "logout":
                    logout();
                    break;
                case "friends":
                    userFriends();
                    break;
                case "friends_month":
                    if(args.length == 2)
                        userFriendsFiltered(Integer.parseInt(args[1]));
                    else
                        System.out.println("Numar invalid de arumente");
                    break;
                case "friends_year":
                    if(args.length == 2)
                        userFriendsFilteredYear(Integer.parseInt(args[1]));
                    else
                        System.out.println("Numar invalid de arumente");
                    break;
                case "conversation":
                    if(args.length == 2)
                        getConversation(args[1]);
                    else
                        System.out.println("Numar invalid de arumente");
                    break;
                case "send":
                    if(args.length >= 3) {
                        ArrayList<String> rez = new ArrayList<>();
                        for(int i = 2;i < args.length; i ++){
                            rez.add(args[i]);
                        }
                        sendMessage(args[1], rez);
                    }
                    else
                        System.out.println("Numar invalid de arumente");
                    break;
                case "reply":
                    if(args.length >= 4) {
                        ArrayList<String> rez = new ArrayList<>();
                        for(int i = 3;i < args.length; i ++){
                            rez.add(args[i]);
                        }
                        sendReplyMessage(args[1], args[2], rez);
                    }
                    else
                        System.out.println("Numar invalid de arumente");
                    break;
                case "fr_req":
                    processFriendRequests();
                    break;
                case "send_req":
                    if(args.length == 2)
                        sendFrReq(args[1]);
                    else
                        System.out.println("Numar invalid de arumente");
                    break;
                case "respond":
                    if(args.length == 3)
                        respond(args[1], Status.valueOf(args[2]));
                    else
                        System.out.println("Numar invalid de arumente");
                    break;
                case "del_req":
                    if(args.length == 2)
                        delFrReq(args[1]);
                    else
                        System.out.println("Numar invalid de arumente");
                    break;
                case "del_mes":
                    if(args.length == 2)
                        del_mes(args[1]);
                    else
                        System.out.println("Numar invalid de arumente");
                    break;
                default:
                    System.out.println("Comanda inexistenta");
            }
        }
    }
}
