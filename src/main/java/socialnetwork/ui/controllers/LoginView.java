package socialnetwork.ui.controllers;

import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import socialnetwork.domain.Utilizator;
import socialnetwork.service.FriendshipService;
import socialnetwork.service.UtilizatorService;

public class LoginView {

    private UtilizatorService service;
    private FriendshipService fr_serv;

    @FXML
    TextField textField;

    @FXML
    Button button;

    @FXML
    TextField firstName;

    @FXML
    TextField secondName;

    @FXML
    Button create;

    @FXML
    public void initialize(){
        button.setOnMouseClicked(e -> {
            try {
                service.login_user(textField.getText());
                fr_serv.login_user(textField.getText());
                System.out.println(service.getCurrentUser().display_name());
            }
            catch(Exception ex){
                Alert a = new Alert(Alert.AlertType.ERROR);
                a.setTitle(ex.toString());
                a.show();
            }
        });
        create.setOnMouseClicked(e -> {
            try{
                service.addUtilizator(new Utilizator(firstName.getText(), secondName.getText()));
            }
            catch(Exception ex){
                Alert a = new Alert(Alert.AlertType.ERROR);
                a.setTitle(ex.toString());
                a.show();
            }
        });

    }

    public void setService(UtilizatorService serv){
        this.service = serv;
    }
    public void setService(FriendshipService serv){
        this.fr_serv = serv;
    }
}
