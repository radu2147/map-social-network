package socialnetwork.ui.controllers;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import socialnetwork.Utils;
import socialnetwork.domain.FriendRequest;
import socialnetwork.domain.Status;
import socialnetwork.domain.Tuple;
import socialnetwork.domain.UserDTO;
import socialnetwork.service.FriendshipService;
import socialnetwork.service.UtilizatorService;

import java.util.List;

public class UserView {
    ObservableList<UserDTO> list = FXCollections.observableArrayList();
    private UtilizatorService service;
    private FriendshipService fr_serv;

    @FXML
    Label userField;

    @FXML
    TextField friendSearch;

    @FXML
    ListView<UserDTO> listView;

    @FXML
    Button button;

    @FXML
    Button friendRequest;

    @FXML
    Label status;

    @FXML
    Button logout;

    @FXML
    Button check;

    @FXML
    Button accept;

    @FXML
    Button reject;

    @FXML
    Button remove;

    @FXML
    Button show;

    @FXML
    public void initialize(){
        if(service != null) {
            if (!service.anonymousSession()) {
                button.setOnMouseClicked(event -> {
                    List<UserDTO> arr = Utils.iterablToList(fr_serv.userFriendsFilteredName(friendSearch.getText()));
                    if(!arr.isEmpty())
                        list.setAll(arr);
                    else {
                        List<UserDTO> arr2 = Utils.iterablToList(service.getNonMatchingFriends(friendSearch.getText()));
                        list.setAll(arr2);
                    }
                    listView.setItems(list);

                });
                show.setOnMouseClicked(e -> {
                    list.setAll(fr_serv.getAllRequests());
                    listView.setItems(list);
                });
                logout.setOnMouseClicked(e -> {
                    service.logout();
                    fr_serv.logout();
                    clear();
                });
                userField.setText(service.getCurrentUser().display_name());
                friendRequest.setOnMouseClicked(event ->{
                    if(!listView.getSelectionModel().getSelectedItems().isEmpty()){
                        fr_serv.sendFrReq(listView.getSelectionModel().getSelectedItems().get(0).toString());
                        status.setText("Sent request");
                    }
                });
                check.setOnMouseClicked(e -> {
                    if(listView.getSelectionModel().getSelectedItems().isEmpty()){
                        Alert a = new Alert(Alert.AlertType.ERROR);
                        a.setTitle("Select a user first");
                        a.show();
                        return;
                    }
                    if(fr_serv.areFriends(listView.getSelectionModel().getSelectedItems().get(0).toString())){
                        status.setText("FRIENDS");
                    }
                    else{
                        FriendRequest ax = fr_serv.getFrReq(listView.getSelectionModel().getSelectedItems().get(0).toString());
                        if(ax != null){
                            status.setText(ax.getStatus().toString());
                        }
                        else{
                            status.setText("NOT FRIENDS");
                        }
                    }
                });
                accept.setOnMouseClicked(e -> {
                    if(listView.getSelectionModel().getSelectedItems().isEmpty()){
                        Alert a = new Alert(Alert.AlertType.ERROR);
                        a.setTitle("Select a user first");
                        a.show();
                        return;
                    }
                    try {
                        fr_serv.respond_req(listView.getSelectionModel().getSelectedItems().get(0).toString(), Status.ACCEPTED);
                    }
                    catch(Exception ex){
                        Alert a = new Alert(Alert.AlertType.ERROR);
                        a.setTitle(ex.toString());
                        a.show();
                    }
                });
                reject.setOnMouseClicked(e -> {
                    if(listView.getSelectionModel().getSelectedItems().isEmpty()){
                        Alert a = new Alert(Alert.AlertType.ERROR);
                        a.setTitle("Select a user first");
                        a.show();
                        return;
                    }
                    try {
                        fr_serv.respond_req(listView.getSelectionModel().getSelectedItems().get(0).toString(), Status.REJECTED);
                    }
                    catch(Exception ex){
                        Alert a = new Alert(Alert.AlertType.ERROR);
                        a.setTitle(ex.toString());
                        a.show();
                    }
                });
                remove.setOnMouseClicked(e -> {
                    if(listView.getSelectionModel().getSelectedItems().isEmpty()){
                        Alert a = new Alert(Alert.AlertType.ERROR);
                        a.setTitle("Select a user first");
                        a.show();
                        return;
                    }
                    try{
                        fr_serv.del_prietenie(new Tuple<>(listView.getSelectionModel().getSelectedItems().get(0).getId(), service.getCurrentUser().getId()));
                    }
                    catch(Exception ex){
                        Alert a = new Alert(Alert.AlertType.ERROR);
                        a.setTitle(ex.toString());
                        a.show();
                    }
                });
            }
        }
    }
    public void setService(UtilizatorService userServ){
        this.service = userServ;
    }
    public void setService(FriendshipService userServ){
        this.fr_serv = userServ;
    }

    private void clear(){
        listView.setItems(null);
        friendSearch.setText("");
    }
}