package socialnetwork;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

public class Utils {
    public static <Type> List<Type> iterablToList(Iterable<Type> iterable){
        return StreamSupport.stream(iterable.spliterator(), false).collect(Collectors.toList());
    }
}
