package socialnetwork.service;

import socialnetwork.domain.*;
import socialnetwork.domain.validators.Validator;
import socialnetwork.repository.Repository;
import socialnetwork.service.exceptions.*;
import sun.nio.ch.Util;

import java.time.LocalDateTime;
import java.time.Month;
import java.util.*;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

public class FriendshipService{
    private final Repository<Tuple<Long, Long>, Prietenie> repo;
    private final Repository<Tuple<Utilizator, Utilizator>, FriendRequest> req_repo;
    private final Repository<Long, Utilizator> user_repo;
    private final Validator<String> validatorLogin;
    private Utilizator logged_user;

    public FriendshipService(Repository<Tuple<Long, Long>, Prietenie> repo, Repository<Long, Utilizator> user_repo, Repository<Tuple<Utilizator, Utilizator>, FriendRequest> req_repo,Validator<String> validatorLogin){
        this.repo = repo;
        this.user_repo = user_repo;
        this.validatorLogin = validatorLogin;
        this.req_repo = req_repo;
        logged_user = null;
    }

    /**
     * Functie accesata de UI pentru numarul de comunitati
     * @return
     *      integer, representing number of communities
     */
    public int numberCommunities(){
        return dfsCommNumber();
    }

    /**
     * Functie accesata de UI pentru cea mai sociabila comunnitate
     * @return
     *          integer, representing the longest path in the communities
     */
    public List<Long> mostSociableComm(){
        return mostSociableCommunity2();
    }

    /**
     * Functie ce adauga prietenii in lista userilor
     * @return
     *      Iterable de utilizator cupieteniile adaugate
     */
    private Iterable<Utilizator> getUsersWithFriends(){
        Iterable<Utilizator> all = user_repo.findAll();
        for(Prietenie pr: repo.findAll()){
            Utilizator x = null, y = null;
            for(Utilizator user: all){
                if(user.getId() == pr.getId().getLeft()){
                    x = user;
                }
                if(user.getId() == pr.getId().getRight()){
                    y = user;
                }
            }
            x.addFriend(y);
            y.addFriend(x);

        }
        return all;
    }

    /**
     * Functie ce sterge memory leakurile din db
     * @param id
     */
    private void cleanLeaks(Tuple<Long, Long> id){
        ArrayList<Tuple<Utilizator, Utilizator>> arr = new ArrayList<>();
        for(FriendRequest frreq: req_repo.findAll()){
            if(frreq.getId().getLeft().getId().equals(id.getLeft()) && frreq.getId().getRight().getId().equals(id.getRight()) || frreq.getId().getRight().getId().equals(id.getLeft()) && frreq.getId().getRight().getId().equals(id.getLeft())){
                arr.add(frreq.getId());
            }
        }
        for (Tuple<Utilizator, Utilizator> friendRequest : arr) {
            req_repo.delete(friendRequest);
        }
    }

    /**
     *
     * @param id
     *      Long- the id of a user
     * @return
     * Returneaza un utilizator cu prietenii
     */
    private Utilizator findUserWithFriends(Long id){
        Iterable<Utilizator> all = getUsersWithFriends();
        for (Utilizator utilizator : all) {
            if(utilizator.getId() == id){
                return utilizator;
            }
        }
        return null;
    }

    /**
     * Seteaza data prieteniei creeaza prietenia dintre useri si salveaza prietenia
     * @param prietenie
     * @return
     */
    public Prietenie add_friendship(Prietenie prietenie){
        prietenie.setDate(LocalDateTime.now());
        return repo.save(prietenie);
    }

    /**
     * Sterge prietenie si actualizaeaza lista de prieteni
     * @param id
     *      Long
     * @return
     *      Prietenie object
     * @throws FriendDoesNotExist
     *      daca id-urile prietenilor nu exista
     */
    public Prietenie del_prietenie(Tuple<Long,Long> id){
        Utilizator u1 = user_repo.findOne(id.getLeft());
        Utilizator u2 = user_repo.findOne(id.getRight());
        if(u1 == null || u2 == null)
            throw new FriendDoesNotExist("Id-ul unui prieten nu exista");
        u1.getFriends().remove(u2);
        u2.getFriends().remove(u1);
        Prietenie a = repo.delete(id);
        if(a != null)
            cleanLeaks(id);
        return a;
    }

    /**
     * returns all the elements from the repository
     * @return
     *      Iterable of friends type
     */
    public Iterable<Prietenie> get_all(){
        return repo.findAll();
    }

    /**
     * Help function for dfs on number of communities dfs
     * @param start
     *      Utilizator
     * @param map
     *      DFS Map
     * @return
     */
    public void visitDFS(Utilizator start, HashMap<Long, Boolean> map){
        Stack<Utilizator> stack = new Stack<>();
        stack.push(start);
        while(!stack.empty()){
            Utilizator e = stack.pop();
            map.put(e.getId(), true);
            for(Utilizator x: e.getFriends()){
                if(!map.get(x.getId())) {
                    stack.push(x);
                }
            }
        }
    }

    private Long extractId(String username){
        return Long.valueOf(username.split("_")[2]);
    }

    public void login_user(String username){
        validatorLogin.validate(username);
        if(logged_user == null){
            logged_user = user_repo.findOne(extractId(username));
        }
        else{
            throw new ExistingUserAlreadyLoggedIn("Existing user already logged in");
        }

    }
    public void logout(){
        if(logged_user != null){
            logged_user = null;
            return;
        }
        throw new NoUserLoggedIn("No user is logged in");
    }

    /**
     * Number of communities existing in the friendship graph
     * @return
     *      integer value representing the number of communities
     */
    public int dfsCommNumber(){
        int c = 0;
        HashMap<Long, Boolean> map = new HashMap<>();
        for(Utilizator el: getUsersWithFriends()){
            map.put(el.getId(), false);
        }
        while(true){
            boolean able = false;
            for(long id: map.keySet()){
                if(!map.get(id)){
                    visitDFS(findUserWithFriends(id), map);
                    c += 1;
                    able = true;
                }
            }
            if(!able){
                break;
            }
        }
        return c;
    }

    public boolean areFriends(String username){
        return isFriend(extractId(username), logged_user.getId()) || isFriend(logged_user.getId(), extractId(username));
    }

    public Iterable<UserDTO> userFriends(){
        return userFriendsFilter(e -> logged_user.getId().equals(e.getId().getLeft()) || logged_user.getId().equals(e.getId().getRight()));
    }

    public Iterable<UserDTO> userFriendsFiltered(int month){
        return userFriendsFilter(e -> (logged_user.getId().equals(e.getId().getLeft()) || logged_user.getId().equals(e.getId().getRight())) && e.getDate().getMonth().equals(Month.of(month)));
    }

    public Iterable<UserDTO> userFriendsFilteredName(String username){
        return userFriendsFilter(e -> {
            if(logged_user.getId().equals(e.getId().getLeft())){
                return user_repo.findOne(e.getId().getRight()).display_name().contains(username);
            }
            else if(logged_user.getId().equals(e.getId().getRight())){
                return user_repo.findOne(e.getId().getLeft()).display_name().contains(username);
            }
            return false;
        });
    }
    private Iterable<UserDTO> userFriendsFilter(Predicate<Prietenie> function){
        if(logged_user == null) throw new NoUserLoggedIn("No user is logged in");
        return StreamSupport.stream(repo.findAll().spliterator(), false)
                .filter(function::test)
                .map(
                        e -> {
                            Utilizator u = user_repo.findOne((logged_user.getId().equals(e.getId().getLeft())) ? e.getId().getRight() : e.getId().getLeft());
                            return new UserDTO(u.getId(), u.getFirstName(), u.getLastName(), e.getDate());
                        }
                )
                .collect(Collectors.toList());
    }

    public Iterable<UserDTO> userFriendsFilteredYear(int year){
        return userFriendsFilter(e -> (logged_user.getId().equals(e.getId().getLeft()) || logged_user.getId().equals(e.getId().getRight())) && e.getDate().getYear() == year);
    }

    /**
     * Returns the maximum path starting from a given node
     * @param u
     *      Utilizator object
     * @param c
     *      integer
     * @param map
     *      HashMap
     * @return
     *      integer representing the component with maximum path
     */

    public List<Long> findMaxi2(Utilizator u, List<Long> c, HashMap<Long, Boolean> map){
        List<Long> maxi = new ArrayList<>();
        maxi.addAll(c);
        map.put(u.getId(), true);
        for(Utilizator a: u.getFriends()){
            if(!map.get(a.getId())) {
                c.add(a.getId());
                List<Long> maxLoc = findMaxi2(a, c, map);
                c.remove(a.getId());
                if(maxLoc.size() > maxi.size()){
                    maxi.clear();
                    maxi.addAll(maxLoc);
                }
            }
        }
        return maxi;
    }

    /**
     * Finds the most sociable community and returns the longest path between them
     *
     * @return
     *      longest path in the most sociable community
     */

    public List<Long> mostSociableCommunity2(){
        List<Long> maxi = new ArrayList<>();

        for(Utilizator u: getUsersWithFriends()){
            HashMap<Long, Boolean> map = new HashMap<>();
            for(Utilizator ur: getUsersWithFriends()){
                map.put(ur.getId(), false);
            }
            List<Long> x = new ArrayList<>();
            x.add(u.getId());
            List<Long> a = findMaxi2(u, x, map);
            if(maxi.size() < a.size()){
                maxi = a;
            }
        }
        return maxi;
    }


    public Iterable<FriendRequest> process(){
        if(logged_user == null) throw new NoUserLoggedIn("No user lgged in");
        return StreamSupport.stream(req_repo.findAll().spliterator(), false)
                .filter(e -> e.getId().getLeft().getId() == logged_user.getId() || e.getId().getRight().getId() == logged_user.getId())
                .map(e -> {
                    e.setId(new Tuple<>(user_repo.findOne(e.getId().getLeft().getId()), user_repo.findOne(e.getId().getRight().getId())));
                    return e;
                })
                .collect(Collectors.toList());
    }

    private boolean isFriend(Long id1,Long id){
        return repo.findOne(new Tuple<>(id1, id)) != null;
    }

    public void sendFrReq(String username){
        if(logged_user == null) throw new NoUserLoggedIn("No user lgged in");
        Utilizator searched = user_repo.findOne(extractId(username));
        if(searched == null)
            throw new UserNotExists("User does not exist");
        if(isFriend(logged_user.getId(),extractId(username)) || req_repo.findOne(new Tuple<>(searched, logged_user)) != null) {
            throw new FriendshipExists("Friendships exists");
        }
        req_repo.save(new FriendRequest(logged_user, searched, Status.PENDING));
    }

    public void delFrReq(String username){
        if(logged_user == null) throw new NoUserLoggedIn("No user lgged in");
        Utilizator searched = user_repo.findOne(extractId(username));
        if(searched == null)
            throw new UserNotExists("User does not exist");
        if(req_repo.delete(new Tuple<>(searched, logged_user)) == null)
            req_repo.delete(new Tuple<>(logged_user,searched));
    }

    public void respond_req(String username, Status status){
        if(logged_user == null) throw new NoUserLoggedIn("No user logged in");
        Utilizator user = user_repo.findOne(extractId(username));
        if(user == null)
            throw new UserNotExists("User not exists");
        FriendRequest req = req_repo.findOne(new Tuple<>(user, logged_user));
        if(req != null){
            if(req.getStatus() == Status.PENDING) {
                req.setStatus(status);
                if (status == Status.ACCEPTED)
                    add_friendship(new Prietenie(new Tuple<>(user.getId(), logged_user.getId())));
                req_repo.update(req);

            }
            else{
                throw new RequestProcessed("Request already processed");
            }
            return;
        }
        throw new FriendDoesNotExist("Request does not exist");
    }

    public FriendRequest getFrReq(String username){
        if(logged_user == null)
            throw new RuntimeException("User not logged in");
        FriendRequest a = req_repo.findOne(new Tuple<>(user_repo.findOne(extractId(username)), logged_user));
        if(a != null)
            return a;
        return req_repo.findOne(new Tuple<>(logged_user, user_repo.findOne(extractId(username))));
    }
    public List<UserDTO> getAllRequests(){
        ArrayList<UserDTO> arr = new ArrayList<>();
        for (Utilizator utilizator : user_repo.findAll()) {
            FriendRequest a = getFrReq(utilizator.display_name());
            if(a != null)
                arr.add(new UserDTO(utilizator.getId(), utilizator.getFirstName(), utilizator.getLastName(), null));
        }
        return arr;
    }
}
