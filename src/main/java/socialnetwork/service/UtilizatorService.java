package socialnetwork.service;

import socialnetwork.domain.*;
import socialnetwork.domain.validators.Validator;
import socialnetwork.observers.Observable;
import socialnetwork.repository.Repository;
import socialnetwork.service.exceptions.ExistingUserAlreadyLoggedIn;
import socialnetwork.service.exceptions.NoUserLoggedIn;
import socialnetwork.service.exceptions.UserNotExists;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;


public class UtilizatorService extends Observable {
    private final Repository<Long, Utilizator> repo;
    private final Repository<Tuple<Long, Long>, Prietenie> fr_repo;
    private final Repository<Long, Message> mess_repo;
    private Utilizator logged_user;
    private final Validator<String> validatorLogin;
    private Repository<Tuple<Utilizator, Utilizator>, FriendRequest> req_repo;

    public UtilizatorService(Repository<Long, Utilizator> repo, Repository<Tuple<Long, Long>, Prietenie> fr_repo, Repository<Long, Message> mess_repo, Repository<Tuple<Utilizator, Utilizator>, FriendRequest> req_repo,Validator<String> validator) {
        this.repo = repo;
        this.fr_repo = fr_repo;
        this.mess_repo = mess_repo;
        this.req_repo = req_repo;
        this.logged_user = null;
        this.validatorLogin = validator;
    }

    /**
     * Saves the user in memory
     *
     * @param messageTask
     * @return
     *      Return result of repo.save function
     */
    public Utilizator addUtilizator(Utilizator messageTask) {
        return repo.save(messageTask);
    }

    /**
     * Removes the user and all its friendship object.
     *
     * @param id
     * @return
     *      returns the eliminated utilizator
     */
    public Utilizator removeUtilizator(Long id){

        Utilizator u = repo.delete(id);
        ArrayList<Prietenie> arr = new ArrayList<>();
        for(Prietenie el: fr_repo.findAll()){
            if(el.getId().getLeft().equals(id) || el.getId().getRight().equals(id)){
                arr.add(el);
            }
        }
        ArrayList<Message> arr2 = new ArrayList<>();
        for(Message m: mess_repo.findAll()){
            if(m.getFrom().getId() == id || m.getTo().get(0).getId() == id)
                arr2.add(m);
        }

        ArrayList<FriendRequest> arr3 = new ArrayList<>();
        for(FriendRequest fr: req_repo.findAll()){
            if(fr.getId().getLeft().getId() == id || fr.getId().getRight().getId() == id)
                arr3.add(fr);
        }
        arr3.forEach(e -> req_repo.delete(e.getId()));
        for(Message m: mess_repo.findAll()){
            if(m.getFrom().getId() == id || m.getTo().get(0).getId() == id)
                arr2.add(m);
        }
        arr2.forEach(e -> mess_repo.delete(e.getId()));
        arr.forEach(pr->{
            fr_repo.delete(pr.getId());
            Long idToDel = (pr.getId().getLeft().equals(id)) ? pr.getId().getRight() : pr.getId().getLeft();
            repo.findOne(idToDel).getFriends().remove(u);
        });

        return u;
    }

    /**
     * Function
     * @return
     */
    private Iterable<Utilizator> getUsersWithFriends(){
        Iterable<Utilizator> all = repo.findAll();
        for(Prietenie pr: fr_repo.findAll()){
            Utilizator x = null, y = null;
            for(Utilizator user: all){
                if(user.getId().equals(pr.getId().getLeft())){
                    x = user;
                }
                if(user.getId().equals(pr.getId().getRight())){
                    y = user;
                }
            }
            x.addFriend(y);
            y.addFriend(x);

        }
        return all;
    }

    /**
     * Returns all the Utilizator objects
     * @return
     *      Iterable object from repository
     */
    public Iterable<Utilizator> getAll(){
        return getUsersWithFriends();
    }

    private Long extractId(String username){
        return Long.valueOf(username.split("_")[2]);
    }

    public void login_user(String username){
        validatorLogin.validate(username);
        if(anonymousSession()){
            logged_user = repo.findOne(extractId(username));
            if(logged_user == null)
                throw new UserNotExists("User does not exists");

            update();
        }
        else{
            throw new ExistingUserAlreadyLoggedIn("Existing user already logged in");
        }

    }

    public Iterable<UserDTO> getNonMatchingFriends(String username){
        return filter(e -> e.display_name().contains(username) && e.getId() != logged_user.getId());
    }

    private Iterable<UserDTO> filter(Predicate<Utilizator> function){
        if(logged_user == null) throw new NoUserLoggedIn("No user is logged in");
        return StreamSupport.stream(repo.findAll().spliterator(), false)
                .filter(function::test)
                .map(
                        e -> new UserDTO(e.getId(), e.getFirstName(), e.getLastName())
                )
                .collect(Collectors.toList());
    }

    public void logout(){
        if(logged_user != null){
            logged_user = null;
            update();
            return;
        }
        throw new NoUserLoggedIn("No user is logged in");
    }

    public boolean anonymousSession(){
        return logged_user == null;
    }

    public Utilizator getCurrentUser(){
        return logged_user;
    }

    public Iterable<Message> getConversation(String username){
        if(anonymousSession()) throw new NoUserLoggedIn("No user is logged in");
        validatorLogin.validate(username);
        Iterable<Message> mess = mess_repo.findAll();
        List<Message> all = StreamSupport.stream(mess.spliterator(), false)
                .filter(e -> e.getFrom().getId() == extractId(username) || extractId(username) == e.getTo().get(0).getId())
                .map(e -> {
                    e.setFrom(repo.findOne(e.getFrom().getId()));
                    e.addTo(repo.findOne(e.getTo().get(0).getId()));
                    if(e instanceof ReplyMessage){
                        ((ReplyMessage) e).setReplyTo(mess_repo.findOne(((ReplyMessage) e).getReplyTo().getId()));
                    }
                    return e;
                })
                .sorted((o1, o2) -> {
                    if(o1.getDate().isAfter(o2.getDate())){
                        return 1;
                    }
                    return -1;
                })
                .collect(Collectors.toList());
        return all;
    }

    public void send_message(String text, ArrayList<String> usernames){
        if(anonymousSession()) throw new NoUserLoggedIn("No user is logged in");
        for(String username: usernames) {
            validatorLogin.validate(username);
            Message toSend = new Message(text, LocalDateTime.now());
            toSend.setFrom(logged_user);
            Utilizator recver = repo.findOne(extractId(username));
            if(recver == null)
                throw new UserNotExists("User does not exist");
            toSend.addTo(recver);
            mess_repo.save(toSend);
        }
    }

    public void sendReplyTo(String text, Long messId, ArrayList<String> usernames){
        if(anonymousSession()) throw new NoUserLoggedIn("No user is logged in");
        for(String username: usernames) {
            validatorLogin.validate(username);
            Message m = mess_repo.findOne(messId);
            ReplyMessage toSend = new ReplyMessage(text, LocalDateTime.now(), m);
            toSend.setFrom(logged_user);
            Utilizator recver = repo.findOne(extractId(username));
            if(recver == null)
                throw new UserNotExists("User does not exist");
            toSend.addTo(recver);

            mess_repo.save(toSend);
        }
    }

    public Message del_message(Long id){
        if(anonymousSession()) throw new NoUserLoggedIn("No user is logged in");
        return mess_repo.delete(id);

    }

}
