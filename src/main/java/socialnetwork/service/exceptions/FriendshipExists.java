package socialnetwork.service.exceptions;

public class FriendshipExists extends RuntimeException{
    public FriendshipExists(){

    }
    public FriendshipExists(String message){
        super(message);
    }
}
