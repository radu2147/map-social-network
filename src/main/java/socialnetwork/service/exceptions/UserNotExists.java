package socialnetwork.service.exceptions;

public class UserNotExists extends RuntimeException{
    public UserNotExists(String mess){
        super(mess);
    }
}
