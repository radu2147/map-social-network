package socialnetwork.repository.database;

import socialnetwork.domain.Utilizator;
import socialnetwork.domain.validators.Validator;

import java.sql.*;

public class UtilizatorDbRepository extends AbstractDbRepository<Long, Utilizator> {

    public UtilizatorDbRepository(String url, String username, String password, Validator<Utilizator> validator) {
        super(url, username, password, validator);
    }
    @Override
    public Utilizator findOne(Long aLong) {
        String sqlQuerry = "SELECT * from users where id=" + aLong;
        setQuery(sqlQuerry);
        return super.findOne(aLong);
    }

    @Override
    public Iterable<Utilizator> findAll() {
        String querry = "SELECT * from users";
        setQuery(querry);
        return super.findAll();
    }

    @Override
    public Utilizator save(Utilizator entity) {
        String sqlQuery = "insert into users(first_name, last_name) values('" + entity.getFirstName() + "','" + entity.getLastName() + "');";
        setQuery(sqlQuery);
        return super.save(entity);
    }

    @Override
    public Utilizator delete(Long aLong) {
        String sqlQuery = "delete from users where id=" + aLong;
        setQuery(sqlQuery);
        return super.delete(aLong);

    }

    @Override
    public Utilizator update(Utilizator entity) {
        String sqlQuery = "update users set first_name='" + entity.getFirstName() + "', last_name='" + entity.getLastName() + "' where id=" + entity.getId() + ";";
        setQuery(sqlQuery);
        return super.save(entity);
    }

    @Override
    protected Utilizator mapDbObject(ResultSet resultSet) throws SQLException {
        Long id = resultSet.getLong("id");
        String firstName = resultSet.getString("first_name");
        String lastName = resultSet.getString("last_name");

        Utilizator utilizator = new Utilizator(firstName, lastName);
        utilizator.setId(id);
        return utilizator;
    }
}
