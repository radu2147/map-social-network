package socialnetwork.repository.database;

import socialnetwork.domain.*;
import socialnetwork.domain.validators.Validator;
import socialnetwork.repository.Repository;

import java.sql.*;
import java.util.HashSet;
import java.util.Set;

public abstract class AbstractDbRepository<ID, E extends Entity<ID>> implements Repository<ID, E> {

    private String query;
    private String url;
    private String username;
    private String password;
    private Validator<E> validator;

    public AbstractDbRepository(String url, String username, String password, Validator<E> validator){

        this.url = url;
        this.username = username;
        this.password = password;
        this.validator = validator;
    }

    public AbstractDbRepository() {
        super();
    }

    @Override
    public E findOne(ID id) {
        try(Connection connection = DriverManager.getConnection(url, username, password);
            PreparedStatement statement = connection.prepareStatement(query);
            ResultSet resultSet = statement.executeQuery()){
            if(resultSet.next()) {
                return mapDbObject(resultSet);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public Iterable<E> findAll() {
        Set<E> prSet = new HashSet<>();
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement statement = connection.prepareStatement(query);
             ResultSet resultSet = statement.executeQuery()) {

            while (resultSet.next()) {
                prSet.add(mapDbObject(resultSet));
            }
            return prSet;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return prSet;
    }

    @Override
    public E save(E entity) {
        validator.validate(entity);
        String save = query;
        E pr = findOne(entity.getId());
        query = save;
        if(pr == null){
            try(Connection connection = DriverManager.getConnection(url, username, password);
                PreparedStatement statement = connection.prepareStatement(query)
            ){
                statement.executeUpdate();
                return null;
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return entity;
    }

    @Override
    public E delete(ID id) {
        String save = query;
        E u = findOne(id);
        query = save;
        if(u != null){
            try(Connection connection = DriverManager.getConnection(url, username, password);
                PreparedStatement statement = connection.prepareStatement(query);
            ){
                statement.executeUpdate();
                return u;
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    @Override
    public E update(E entity) {
        validator.validate(entity);
        String save = query;
        E u = findOne(entity.getId());
        query = save;
        if(u != null){
            try(Connection connection = DriverManager.getConnection(url, username, password);
                PreparedStatement statement = connection.prepareStatement(query);
            ){
                statement.executeUpdate();
                return entity;
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    @Override
    public int size() {
        return 0;
    }

    public void setQuery(String querry){
        this.query = querry;
    }

    protected abstract E mapDbObject(ResultSet resultSet) throws SQLException;
}
