package socialnetwork.repository.database;

import socialnetwork.domain.Prietenie;
import socialnetwork.domain.Tuple;
import socialnetwork.domain.validators.Validator;

import java.sql.*;
import java.time.LocalDateTime;

public class FriendshipDbRepository extends AbstractDbRepository<Tuple<Long, Long>, Prietenie> {

    public FriendshipDbRepository(String url, String username, String password, Validator<Prietenie> validator) {
        super(url, username, password, validator);
    }
    @Override
    public Prietenie findOne(Tuple<Long, Long> aLong) {
        String sqlQuerry = "SELECT * from friendships where id1=" + aLong.getLeft() + " and id2=" + aLong.getRight();
        setQuery(sqlQuerry);
        return super.findOne(aLong);
    }

    @Override
    public Iterable<Prietenie> findAll() {
        String querry = "SELECT * from friendships";
        setQuery(querry);
        return super.findAll();
    }

    @Override
    public Prietenie save(Prietenie entity) {
        String sqlQuery = "insert into friendships(id1, id2, date) values(" + entity.getId().getLeft() + "," + entity.getId().getRight() + ", '" + entity.getDate().toString() + "');";
        setQuery(sqlQuery);
        return super.save(entity);
    }

    @Override
    public Prietenie delete(Tuple<Long, Long> aLong) {
        String sqlQuery = "delete from friendships where id1=" + aLong.getLeft() + " and id2="+aLong.getRight();
        setQuery(sqlQuery);
        return super.delete(aLong);

    }

    @Override
    public Prietenie update(Prietenie entity) {
        return null;
    }

    @Override
    protected Prietenie mapDbObject(ResultSet resultSet) throws SQLException {
        Long id1 = resultSet.getLong("id1");
        Long id2 = resultSet.getLong("id2");
        LocalDateTime date = LocalDateTime.parse(resultSet.getString("date"));

        Prietenie pr = new Prietenie(new Tuple<>(id1, id2));
        pr.setDate(date);
        return pr;
    }
}
