package socialnetwork.repository.database;

import socialnetwork.domain.*;
import socialnetwork.domain.validators.Validator;
import java.sql.*;
public class FriendRequestsDbRepository extends AbstractDbRepository<Tuple<Utilizator, Utilizator>, FriendRequest> {


    public FriendRequestsDbRepository(String url, String username, String password, Validator<FriendRequest> validator){

        super(url, username, password, validator);
    }

    @Override
    public FriendRequest findOne(Tuple<Utilizator, Utilizator> aLong) {
        String sqlQuerry = "SELECT * from friendrequests where sender=" + aLong.getLeft().getId() + " and recv=" + aLong.getRight().getId() + ";";
        setQuery(sqlQuerry);
        return super.findOne(aLong);
    }

    @Override
    public Iterable<FriendRequest> findAll() {
        String querry = "SELECT * from friendrequests";
        setQuery(querry);
        return super.findAll();
    }

    @Override
    public FriendRequest save(FriendRequest entity) {
        String sqlQuery = "insert into friendrequests(sender, recv, status) values(" + entity.getId().getLeft().getId() + "," + entity.getId().getRight().getId() + ", '" + entity.getStatus().toString() + "');";
        setQuery(sqlQuery);
        return super.save(entity);
    }

    @Override
    public FriendRequest delete(Tuple<Utilizator, Utilizator> aLong) {
        String sqlQuery = "delete from friendrequests where sender=" + aLong.getLeft().getId() + " and recv="+aLong.getRight().getId();
        setQuery(sqlQuery);
        return super.delete(aLong);
    }

    @Override
    public FriendRequest update(FriendRequest entity) {
        String sqlQuery = "update friendrequests set status='" + entity.getStatus() + "' where sender=" + entity.getId().getLeft().getId() + " and recv=" + entity.getId().getRight().getId() + ";";
        setQuery(sqlQuery);
        return super.update(entity);
    }

    @Override
    protected FriendRequest mapDbObject(ResultSet resultSet) throws SQLException {
        Long id1 = resultSet.getLong("sender");
        Long id2 = resultSet.getLong("recv");
        Status status = Status.valueOf(resultSet.getString("status"));

        FriendRequest pr = new FriendRequest(new Utilizator(id1, "", ""), new Utilizator(id2, "", ""), status);
        return pr;

    }
}
