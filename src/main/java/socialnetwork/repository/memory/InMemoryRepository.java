package socialnetwork.repository.memory;

import socialnetwork.domain.Entity;
import socialnetwork.domain.validators.Validator;
import socialnetwork.repository.Repository;

import java.util.HashMap;
import java.util.Map;

public class InMemoryRepository<ID, E extends Entity<ID>> implements Repository<ID,E> {

    private Validator<E> validator;
    protected Map<ID,E> entities;

    public InMemoryRepository(Validator<E> validator) {
        this.validator = validator;
        entities=new HashMap<ID,E>();
    }

    /**
     *
     * @return
     *      Size of map hashmap
     */
    @Override
    public int size(){
        return entities.size();
    }

    /**
     *
     * @param id -the id of the entity to be returned
     *           id must not be null
     * @return
     *      entity with the given entity.id=id
     */
    @Override
    public E findOne(ID id){
        if (id==null)
            throw new IllegalArgumentException("id must be not null");
        return entities.get(id);
    }

    /**
     *
     * @return
     *          Values of the entities map hashmap
     */
    @Override
    public Iterable<E> findAll() {
        return entities.values();
    }

    /**
     *
     * @param entity
     *         entity must be not null
     * @return
     *         null if the entity was added successfully
     *         entity if the object already exists in the db
     * @throws
     *          IllegalArgumentException if entity is null
     * @throws
     *          socialnetwork.domain.validators.ValidationException if it is not valid
     */
    @Override
    public E save(E entity) {
        if (entity==null)
            throw new IllegalArgumentException("entity must be not null");
        validator.validate(entity);
        if(entities.get(entity.getId()) != null) {
            return entity;
        }
        else entities.put(entity.getId(),entity);
        return null;
    }

    /**
     *
     * @param id
     *      id must be not null
     * @return
     *      entity if the entity was successfully deleted
     *      null if the entity does not exist
     * @throws
     *      IllegalArgumentException if the argument is null
     */
    @Override
    public E delete(ID id) {
        if(id == null)
            throw new IllegalArgumentException("entity id must not be null");
        E entity = entities.get(id);
        entities.remove(id);
        return entity;
    }

    /**
     *
     * @param entity
     *          entity must not be null
     * @return
     *          null if the entity was updated successfully
     *          the entity if it doesn't exist
     * @throws
     *          IllegalArgumentException if entity is null
     * @throws
     *          socialnetwork.domain.validators.ValidationException if entity is not valid
     */
    @Override
    public E update(E entity) {

        if(entity == null)
            throw new IllegalArgumentException("entity must be not null!");
        validator.validate(entity);

        entities.put(entity.getId(),entity);

        if(entities.get(entity.getId()) != null) {
            entities.put(entity.getId(),entity);
            return null;
        }
        return entity;

    }

}
