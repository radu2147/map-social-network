package socialnetwork.domain;

public class FriendRequest extends Entity<Tuple<Utilizator, Utilizator>>{

    private Status status;
    public FriendRequest(Utilizator id1, Utilizator id2, Status status){
        setId(new Tuple<>(id1, id2));
        this.status = status;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }
}
