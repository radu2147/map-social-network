package socialnetwork.domain;

import java.time.LocalDateTime;

public class UserDTO extends Entity<Long>{
    private String first_name;
    private String last_name;
    private LocalDateTime date;

    public UserDTO(String first_name, String last_name, LocalDateTime date){

        this.first_name = first_name;
        this.last_name = last_name;
        this.date = date;
    }

    public UserDTO(Long id, String first_name, String last_name, LocalDateTime date){
        this(first_name, last_name, date);
        this.setId(id);
    }
    public UserDTO(Long id, String first_name, String last_name){
        this.first_name = first_name;
        this.last_name = last_name;
        this.setId(id);
    }

    public String getFirst_name() {
        return first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public LocalDateTime getDate() {
        return date;
    }

    @Override
    public String toString() {
        return first_name + "_" + last_name + "_" + getId();
    }
}
