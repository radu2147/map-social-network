package socialnetwork.domain;

import java.time.LocalDateTime;


public class Prietenie extends Entity<Tuple<Long,Long>> {

    LocalDateTime date;

    public Prietenie() {
    }

    public Prietenie(Tuple<Long, Long> tup){
        setId(tup);
    }

    /**
     *
     * @return the date when the friendship was created
     */
    public LocalDateTime getDate() {
        return date;
    }
    public void setDate(LocalDateTime time){
        date = time;
    }

    @Override
    public String toString() {
        return getId().getLeft() + " " + getId().getRight() + " " + getDate() + "\n";
    }
}
