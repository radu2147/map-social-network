package socialnetwork.domain;

public enum Status {
    ACCEPTED,
    PENDING,
    REJECTED

}
