package socialnetwork.domain.validators;

import socialnetwork.domain.Utilizator;

public class UtilizatorValidator implements Validator<Utilizator> {
    @Override
    public void validate(Utilizator entity) throws ValidationException {
        StringBuilder m = new StringBuilder();
        if(entity.getId() <= 0){
            m.append("Id cannot be lower than 0\n");
        }
        if(entity.getFirstName() == null){
            m.append("First name cannot be null\n");
        }
        else if(entity.getFirstName().length() <= 2 || entity.getFirstName().length() >= 25){
            m.append("First name must be between 2 and 25 chars\n");
        }
        if(entity.getLastName() == null){
            m.append("Last name cannot be null\n");
        }
        else if(entity.getLastName().length() <= 2 || entity.getLastName().length() >= 25){
            m.append("Last name must be between 2 and 25 chars\n");
        }
        if(!m.toString().equals(""))
            throw new ValidationException(m.toString());

    }
}
