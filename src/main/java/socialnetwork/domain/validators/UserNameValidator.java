package socialnetwork.domain.validators;

public class UserNameValidator implements Validator<String>{
    @Override
    public void validate(String username) throws ValidationException {
        StringBuilder m = new StringBuilder();
        String[] args = username.split("_");
        if(args.length != 3)
            m.append("Username incorect");
        else{
            if(!args[2].matches("[0-9]*")){
                m.append("Id incorect");
            }
        }
        if(m.toString().length() > 0){
            throw new ValidationException(m.toString());
        }
    }
}
