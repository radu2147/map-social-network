package socialnetwork.domain.validators;

import socialnetwork.domain.FriendRequest;

public class RequestValidator implements Validator<FriendRequest> {
    @Override
    public void validate(FriendRequest entity) throws ValidationException {
        StringBuilder s = new StringBuilder();
        if(entity.getId().getRight().getId() == entity.getId().getLeft().getId() || entity.getId().getLeft().getId() < 0 || entity.getId().getRight().getId() < 0)
            s.append("Id-uri gresite");
        if(s.length() > 0){
            throw new ValidationException(s.toString());
        }
    }
}
