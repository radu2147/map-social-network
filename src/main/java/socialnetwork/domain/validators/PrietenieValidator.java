package socialnetwork.domain.validators;

import socialnetwork.domain.Prietenie;

public class PrietenieValidator implements Validator<Prietenie>{
    @Override
    public void validate(Prietenie entity) throws ValidationException {
        StringBuilder msg = new StringBuilder();
        if(entity.getId().getRight() < 1 || entity.getId().getLeft() < 1){
            msg.append("Id-ul nu poate fi mai mic egal ca 0\n");
        }
        if(entity.getId().getRight() == entity.getId().getLeft()){
            msg.append("Id-urile nu pot fi egale intre ele");
        }
        if(!msg.toString().equals("")){
            throw new ValidationException(msg.toString());
        }
    }
}
