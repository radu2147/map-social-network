package socialnetwork.domain;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class Message extends Entity<Long>{
    private Utilizator from;
    private List<Utilizator> to;
    private LocalDateTime date;
    private String text;

    public Message(String text){
        this.text = text;
        this.date = LocalDateTime.now();
        to = new ArrayList<>();
    }

    public Message(String text, LocalDateTime date){
        this(text);
        setDate(date);
    }

    public Utilizator getFrom() {
        return from;
    }

    public void setFrom(Utilizator from) {
        this.from = from;
    }

    public List<Utilizator> getTo() {
        return to;
    }

    public void addTo(Utilizator user) {
        this.to.add(user);
    }

    public LocalDateTime getDate() {
        return date;
    }

    public void setDate(LocalDateTime date) {
        this.date = date;
    }

    public String getText() {
        return text;
    }

    @Override
    public String toString() {
        return getId() + " From: " + getFrom().display_name() + " " + getText() + " " + getDate();
    }
}
