package socialnetwork.domain;

import java.time.LocalDateTime;

public class ReplyMessage extends Message{

    private Message replyTo;

    public ReplyMessage(String text, Message replyTo) {
        super(text);
        this.replyTo = replyTo;
    }

    public ReplyMessage(String text, LocalDateTime date, Message replyTo) {
        super(text, date);
        this.replyTo = replyTo;
    }

    public Message getReplyTo() {
        return replyTo;
    }
    public void setReplyTo(Message m){
        this.replyTo = m;
    }

    @Override
    public String toString() {
        return super.toString() + " " + " replyTo " + ((getReplyTo() != null) ? getReplyTo().getText() : "Message removed");
    }
}
