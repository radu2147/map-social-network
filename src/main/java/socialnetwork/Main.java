package socialnetwork;

import socialnetwork.config.ApplicationContext;
import socialnetwork.domain.*;
import socialnetwork.domain.validators.*;
import socialnetwork.repository.Repository;
import socialnetwork.repository.database.*;
import socialnetwork.service.FriendshipService;
import socialnetwork.service.UtilizatorService;
import socialnetwork.ui.ConsoleUI;
import socialnetwork.ui.FXUi;


public class Main {

    public static void main(String[] args) {
//        String fileName= ApplicationContext.getPROPERTIES().getProperty("data.socialnetwork.users");
//        Repository<Long,Utilizator> userFileRepository = new UtilizatorFile(fileName
//                , new UtilizatorValidator());


        final String url = ApplicationContext.getPROPERTIES().getProperty("database.socialnetwork.url");
        final String username= ApplicationContext.getPROPERTIES().getProperty("database.socialnetwork.username");
        final String pasword= ApplicationContext.getPROPERTIES().getProperty("database.socialnetwork.password");
        Repository<Long,Utilizator> userFileRepository3 =
                new UtilizatorDbRepository(url,username, pasword,  new UtilizatorValidator());
        Repository<Tuple<Long, Long>, Prietenie> friendshipFileRepository3 = new FriendshipDbRepository(url, username, pasword, new PrietenieValidator());
        Repository<Tuple<Utilizator, Utilizator>, FriendRequest> friendreqsDbRepository3 = new FriendRequestsDbRepository(url, username, pasword, new RequestValidator());
        Repository<Long, Message> messageRepository = new MessageDbRepository(url, username, pasword, new MessageValidator());

        UtilizatorService service = new UtilizatorService(userFileRepository3, friendshipFileRepository3, messageRepository, friendreqsDbRepository3,new UserNameValidator());
        FriendshipService fr_serv = new FriendshipService(friendshipFileRepository3,userFileRepository3,friendreqsDbRepository3,new UserNameValidator());

        new ConsoleUI(service, fr_serv).run();

        //FXUi.main(args);

    }
}


